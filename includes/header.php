<?php
// includes/header.php
require_once __DIR__ . '/auth.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Imobiliária</title>
    <link rel="stylesheet" href="/assets/css/style.css">
    <style>
        /* Estilos CSS podem ser adicionados aqui */
    </style>
</head>
<body>
    <header>
        <div class="container">
            <h1>Imobiliária</h1>
            <nav>
                <ul>
                    <li><a href="/public/index.php">Home</a></li>

                    <?php if (is_logged_in()): ?>
                        <?php if (is_corretor()): ?>
                                <li><a href="/public/cadastro/cadastro_corretor.php">Cadastrar Corretor</a></li>
                                <li><a href="/public/listar_corretores.php">Listar Corretores</a></li>
                                <li><a href="/public/visualizar_carrinhos.php">Carrinhos Clientes</a></li>

                            <li><a href="/public/propriedades_vendidas.php">Vendas</a></li>
                            <li><a href="/public/listar_propriedades.php">Propriedades</a></li>
                        <?php endif; ?>
                        <?php if (is_cliente()): ?>
                            <li><a href="/public/listar_propriedades_cliente.php">Propriedades Disponíveis</a></li>
                            <li><a href="/public/propriedades_compradas.php">Minhas Propriedades</a></li>
                            <li><a href="/public/carrinho.php">Meu Carrinho</a></li>
                        <?php endif; ?>
                        <li><a href="/public/perfil.php">Meu Perfil</a></li>
                        <li><a class="logout" href="/public/logout.php">Logout</a></li>
                    <?php else: ?>
                        <li><a href="/public/login.php">Login</a></li>
                        <li><a href="/public/cadastro/cadastro_cliente.php">Cadastre-se</a></li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </header>
    <div class="container">
