<?php
// includes/auth.php
session_start();

function check_login() {
    if (!isset($_SESSION['user_id'])) {
        header("Location: /public/login.php");
        exit;
    }
}

function is_logged_in() {
    return isset($_SESSION['user_id']);
}

function is_cliente() {
    return isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'cliente';
}

function is_corretor() {
    return isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'corretor';
}

function is_gerente() {
    return isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'gerente';
}
?>
