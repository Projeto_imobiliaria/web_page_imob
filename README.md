A imobiliária Maria de Jesus é responsável por administrar uma variedade de propriedades e atender clientes interessados em compra-las. 
O sistema da imobiliária é composto por várias entidades que mantêm informações cruciais sobre clientes, propriedades, transações e funcionários.

- **Cliente**: 
	Cada cliente é identificado por um ID único(CPF).  

	Os clientes podem se envolver em várias transações de compra ao longo do tempo, tendo como atributo o id do contrato de compra.
	Cliente terá um "carrinho" de interesses, indicando que deseja comprar tal propriedade.
    


- Corretor: 
	Existem 2 tipos de corretores, o padrão(com acesso limitado) e o gerente.
	O corretor gerente é o responsável por efetuar a venda do imovel.
	e ambos corretores podem adicionar imoveis ao banco.
	O gerente irão acessar a tabela de "Carrinho" e selecionar qual será o comprador da propriedade
	Cada corretor é identificado por um ID único(matricula),
	 cargo e  
	 salário. 
	Os gerentes podem estar envolvidos em várias transações de venda. 


Cliente e Corretor são Pessoas.

- **Pessoa**:
	As pessoas são separadas entre corretores e Clientes.
	pessoa eh identificadas por um ID único referenciado a primary key de cliente ou corretor e possui
		nome,
		email,
		telefone(multivalorado),
		senha


- **Propriedade**:  # o ID_unico pode ser um md5 de (CEP+numero+complemento(se não for apt complemento = 0000))
	As propriedades são os imóveis disponíveis para compra. 
	Cada propriedade identificado por um ID único e possui 
		um proprietário(vazio ou cliente), 
		um endereço (CEP, rua, número, complemento, bairro e cidade), 
		um tipo (como Apartamento(Apartamento Duplex, Cobertura, Flat), Bangalô, Casa, Chácara, Terreno),
		preço do imóvel e
		descrição(podendo ser usada para adicionar infos importantes, se existe fonte de agua, vazamento na parede, etc.)
		tamanho
    

- **Imóvel**:  
	Cada propriedade identificado por um ID único, id_imovel, e possui 
		um tipo (Apartamento(Apartamento Duplex, Cobertura, Flat), Bangalô, Casa, Chácara),
		qtd quartos,
		qtd_banheiros
		qtd_suites
		qtd vagas garagem 



- **Terreno**:   
	Cada propriedade identificado por um ID único e possui 
	tipo rural ou urbano




- **Transação**
	As transações são uma abstração para nota fiscal e registro da compra/venda
	Quando o corretor concluir uma venda/transação, um contrato_nf será emitido
	As transações são identificadas por meio de um ID único e possui
		data da transação, 
		valor da venda, 
		além de chaves estrangeiras que fazem referência aos IDs do cliente e da propriedade envolvidos na transação.

- **Contrato**: 
	Um contrato é criado para formalizar uma transação de compra/venda específica. 
	Ele registra a data de início e término do contrato, juntamente com uma chave estrangeira que se relaciona com a transação correspondente.
	
	tem como atributo o id do contrato, id do comprador, id do corretor, id da transação


- **Carrinho de Interesse**: 
	- O cliente, para comprar uma propriedade, demonstra interesse, adicionando dada propriedade em seu carrinho.
	- o carrinho de interesse terá 
	- vários clientes podem demonstrar interesse em uma propriedade e os clientes podem ter interesse em mais de uma propriedade.


a venda de uma propriedade deve ser feita por uma transação, resultando em um contrato de compra.

