<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

// public/efetuar_venda.php

require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once __DIR__ . '/../includes/auth.php';
    check_login();

    if (!is_gerente()) {
        header("Location: /public/index.php");
        exit;
    }

    $carrinho_id = trim($_POST['carrinho_id']);
    $cliente_id = trim($_POST['cliente_id']);
    $propriedade_id = trim($_POST['propriedade_id']);
    $corretor_id = $_SESSION['user_id'];

    $conn = connect_db();

    // Iniciar uma transação
    $conn->begin_transaction();

    try {
        // Inserir o contrato de compra
        $stmt = $conn->prepare("INSERT INTO ContratoCompra (cliente_id, propriedade_id, corretor_id, data) VALUES (?, ?, ?, CURDATE())");
        if (!$stmt) {
            throw new Exception("Erro na preparação da declaração SQL: " . $conn->error);
        }

        $stmt->bind_param("sii", $cliente_id, $propriedade_id, $corretor_id);

        if (!$stmt->execute()) {
            throw new Exception("Erro ao registrar a venda: " . $stmt->error);
        }

        $stmt->close();

        // Remover a propriedade do carrinho
        $stmt = $conn->prepare("DELETE FROM carrinho WHERE id = ?");
        if (!$stmt) {
            throw new Exception("Erro na preparação da declaração SQL: " . $conn->error);
        }

        $stmt->bind_param("i", $carrinho_id);

        if (!$stmt->execute()) {
            throw new Exception("Erro ao remover a propriedade do carrinho: " . $stmt->error);
        }

        $stmt->close();
        $conn->commit();

        $mensagem = "Venda efetuada com sucesso!";
    } catch (Exception $e) {
        $conn->rollback();
        $mensagem = $e->getMessage();
    }

    $conn->close();

    header("Location: /public/gerente_carrinho.php?mensagem=" . urlencode($mensagem));
    exit;
} else {
    header("Location: /public/gerente_carrinho.php?mensagem=" . urlencode("Método de requisição inválido."));
    exit;
}
?>
