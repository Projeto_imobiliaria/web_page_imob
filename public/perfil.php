<?php
// public/perfil_cliente.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (is_cliente()) {
    header("Location: /public/perfil_cliente.php");
} elseif (is_corretor() || is_gerente()) {
  header("Location: /public/perfil_corretor.php");
}

require_once __DIR__ . '/../includes/footer.php';
?>
