<?php
// public/gerente_carrinho.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_gerente()) {
    header("Location: /public/index.php");
    exit;
}

if (isset($_GET['mensagem'])) {
    $mensagem = urldecode($_GET['mensagem']);
    echo "<p>$mensagem</p>";
}

$conn = connect_db();

$stmt = $conn->prepare("
    SELECT c.id as carrinho_id, c.cliente_id, p.id as propriedade_id, p.titulo, p.descricao, p.preco, cl.nome as cliente_nome
    FROM carrinho c
    JOIN propriedades p ON c.propriedade_id = p.id
    JOIN clientes cl ON c.cliente_id = cl.cpf
    JOIN pessoa pe ON cl.pessoa_id = pe.id
");
$stmt->execute();
$stmt->bind_result($carrinho_id, $cliente_id, $propriedade_id, $titulo, $descricao, $preco, $cliente_nome);

$carrinho_items = [];
while ($stmt->fetch()) {
    $carrinho_items[] = [
        'carrinho_id' => $carrinho_id,
        'cliente_id' => $cliente_id,
        'propriedade_id' => $propriedade_id,
        'titulo' => $titulo,
        'descricao' => $descricao,
        'preco' => $preco,
        'cliente_nome' => $cliente_nome
    ];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Propriedades no Carrinho de Clientes</h2>
    <?php if ($mensagem): ?>
        <p><?php echo htmlspecialchars($mensagem); ?></p>
    <?php endif; ?>
    <?php if ($carrinho_items): ?>
        <table>
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Título da Propriedade</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($carrinho_items as $item): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($item['cliente_nome']); ?></td>
                        <td><?php echo htmlspecialchars($item['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($item['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($item['preco']); ?></td>
                        <td>
                            <form action="efetuar_venda.php" method="post">
                                <input type="hidden" name="carrinho_id" value="<?php echo $item['carrinho_id']; ?>">
                                <input type="hidden" name="cliente_id" value="<?php echo $item['cliente_id']; ?>">
                                <input type="hidden" name="propriedade_id" value="<?php echo $item['propriedade_id']; ?>">
                                <button type="submit" onclick="return confirm('Tem certeza que deseja efetuar a venda desta propriedade?');">Efetuar Venda</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhum item encontrado no carrinho de clientes.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
