<?php
// public/listar_corretores.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
// check_login();
//
// // Verifica se o usuário é um gerente
// if (!is_gerente()) {
//     header("Location: /public/index.php");
//     exit;
// }

$conn = connect_db();

// Consulta para obter corretores e suas vendas
$sql = "
SELECT
    p.nome AS corretor_nome,
    p.email AS corretor_email,
    c.matricula,
    COUNT(cc.id) AS total_vendas,
    SUM(pv.preco) AS total_valor_vendas
FROM Corretor c
JOIN Pessoa p ON c.matricula = p.id
LEFT JOIN Propriedade pv ON pv.corretor_id = c.matricula
LEFT JOIN ContratoCompra cc ON cc.propriedade_id = pv.id
GROUP BY c.matricula, p.nome, p.email
ORDER BY total_vendas DESC
";

$result = $conn->query($sql);

$corretores = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $corretores[] = $row;
    }
}

$conn->close();
?>

<main>
    <h2>Lista de Corretores e Suas Vendas</h2>
    <?php if ($corretores): ?>
        <table>
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Matrícula</th>
                    <th>Total de Vendas</th>
                    <th>Total em Valor de Vendas</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($corretores as $corretor): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($corretor['corretor_nome']); ?></td>
                        <td><?php echo htmlspecialchars($corretor['corretor_email']); ?></td>
                        <td><?php echo htmlspecialchars($corretor['matricula']); ?></td>
                        <td>
                            <?php
                            if ($corretor['total_vendas'] > 0) {
                                echo htmlspecialchars($corretor['total_vendas']);
                            } else {
                                echo 'Sem vendas';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($corretor['total_valor_vendas'] > 0) {
                                echo htmlspecialchars(number_format($corretor['total_valor_vendas'], 2, ',', '.'));
                            } else {
                                echo '-';
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhum corretor encontrado.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
