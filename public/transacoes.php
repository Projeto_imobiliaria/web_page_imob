<?php
// public/transacoes.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';


$conn = connect_db();

$stmt = $conn->prepare("
    SELECT cc.data, p.titulo, p.descricao, p.preco, cl.nome as cliente_nome, c.nome as corretor_nome, 'Compra' as tipo
    FROM ContratoCompra cc
    JOIN Propriedade p ON cc.propriedade_id = p.id
    JOIN Cliente cl ON cc.cliente_cpf = cl.cpf
    JOIN Corretor c ON cc.corretor_id = c.matricula
");
$stmt->execute();
$stmt->bind_result($data, $titulo, $descricao, $preco, $cliente_nome, $corretor_nome, $tipo);

$transacoes = [];
while ($stmt->fetch()) {
    $transacoes[] = [
        'data' => $data,
        'titulo' => $titulo,
        'descricao' => $descricao,
        'preco' => $preco,
        'cliente_nome' => $cliente_nome,
        'corretor_nome' => $corretor_nome,
        'tipo' => $tipo
    ];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Transações de Compra e Venda</h2>
    <?php if ($transacoes): ?>
        <table>
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Tipo</th>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Cliente</th>
                    <th>Corretor</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($transacoes as $transacao): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($transacao['data']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['tipo']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['preco']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['cliente_nome']); ?></td>
                        <td><?php echo htmlspecialchars($transacao['corretor_nome']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma transação encontrada.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
