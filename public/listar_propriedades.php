<?php
// public/listar_propriedades.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';

$conn = connect_db();
$corretor_id = $_SESSION['user_id'];
$stmt = $conn->prepare("SELECT id, titulo, descricao, preco FROM Propriedade ");
$stmt->execute();
$stmt->bind_result($id, $titulo, $descricao, $preco);

$propriedades = [];
while ($stmt->fetch()) {
    $propriedades[] = ['id' => $id, 'titulo' => $titulo, 'descricao' => $descricao, 'preco' => $preco];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Propriedades Disponíveis</h2>
    <a href="adicionar_propriedade.php">Adicionar Propriedade</a>
    <?php if ($propriedades): ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propriedades as $propriedade): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                        <td>
                            <a href="editar_propriedade.php?id=<?php echo $propriedade['id']; ?>">Editar</a>
                            <a href="remover_propriedade.php?id=<?php echo $propriedade['id']; ?>">Remover</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma propriedade encontrada.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
