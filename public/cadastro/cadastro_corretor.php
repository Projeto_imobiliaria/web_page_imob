<?php

// Inclui os arquivos necessários
require_once __DIR__ . '/../../includes/auth.php';
require_once __DIR__ . '/../../includes/header.php';
require_once __DIR__ . '/../../includes/db.php';

$mensagem = '';
$mensagem2 = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Validação dos dados do formulário
    $nome = trim($_POST['nome']);
    $matricula = trim($_POST['matricula']);
    $email = trim($_POST['email']);
    $senha = password_hash($_POST['senha'], PASSWORD_DEFAULT); // Hash da senha
    $cargo = $_POST['cargo'];
    $telefones = $_POST['telefones']; // Array de telefones

    // Conexão com o banco de dados
    $conn = connect_db();

    // Verificar se o MATRICULA já existe
    $stmt = $conn->prepare("SELECT matricula FROM Corretor WHERE matricula = ?");
    $stmt->bind_param("i", $matricula);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
//        $stmt->close();
//        $conn->close();
        $mensagem2 = "MATRICULA já cadastrada. Tente novamente.";
        //header("Location: /../../public/index.php?mensagem=" . urlencode("MATRICULA já cadastrada. Tente novamente."));
        //exit;
    }

    $stmt->close();
    // Verificar se o EMAIL já existe
    $stmt = $conn->prepare("SELECT email FROM Pessoa WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
//        $stmt->close();
//        $conn->close();
        $mensagem2 = "EMAIL já cadastrado. Tente novamente.";
        //exit;
    }

    $stmt->close();

    try {
        // Inicia a transação
        $conn->begin_transaction();

        // Inserir na tabela Pessoa
        $stmt = $conn->prepare("INSERT INTO Pessoa (id, nome, email, senha) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("isss", $matricula, $nome, $email, $senha);

        if (!$stmt->execute()) {
            throw new Exception("Erro ao cadastrar pessoa: " . $stmt->error);
        }


        // Inserir na tabela Corretor
        $stmt = $conn->prepare("INSERT INTO Corretor (matricula, cargo) VALUES (?, ?)");
        $stmt->bind_param("is", $matricula, $cargo);

        if (!$stmt->execute()) {
            throw new Exception("Erro ao cadastrar corretor: " . $stmt->error);
        }

        // Inserir na tabela Telefone
        $stmt = $conn->prepare("INSERT INTO Telefone (pessoa_id, telefone) VALUES (?, ?)");

        foreach ($telefones as $telefone) {
            // Verificar se o telefone não está vazio
            if (!empty($telefone)) {
                $stmt->bind_param("is", $matricula, $telefone);

                if (!$stmt->execute()) {
                    throw new Exception("Erro ao cadastrar telefone: " . $stmt->error);
                }
            }
        }

        // Confirma a transação
        $conn->commit();
        $mensagem = "Novo corretor adicionado com sucesso";
    } catch (Exception $e) {
        // Em caso de erro, reverte a transação
        $conn->rollback();
        $mensagem = "Falha ao adicionar corretor: " . $mensagem2 .$e ;
    }

    // Fecha a declaração e a conexão
    $stmt->close();
    $conn->close();

}
?>

<!-- Formulário de Registro de Corretor -->
<main>

    <h2>Cadastro Corretor</h2>
    <?php if ($mensagem): ?>
    <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
        <?php echo htmlspecialchars($mensagem); ?>
    </div>
    <?php endif; ?>

    <form action="cadastro_corretor.php" method="POST">
        <label for="matricula">Matricula:</label><br>
        <input type="number" id="matricula" name="matricula" required><br>

        <label for="nome">Nome:</label><br>
        <input type="text" id="nome" name="nome" required><br>

        <label for="email">Email:</label><br>
        <input type="email" id="email" name="email" required><br>

        <label for="senha">Senha:</label><br>
        <input type="password" id="senha" name="senha" required><br>

        <label for="telefones">Telefones:</label><br>
        <input type="tel" id="telefone1" name="telefones[]" required><br>
        <input type="tel" id="telefone2" name="telefones[]"><br>
        <input type="tel" id="telefone3" name="telefones[]"><br>


        <label for="cargo">Cargo:</label>
        <select id="cargo" name="cargo" required>
          <option value="corretor">Corretor</option>
          <option value="gerente">Gerente</option>

        <?php if (is_gerente()): ?>

          <option value="gerente">Gerente</option>
        <?php endif; ?>

        </select>


        <button type="submit">Registrar</button>
    </form>
</main>


<?php
require_once __DIR__ . '/../../includes/footer.php';
?>
