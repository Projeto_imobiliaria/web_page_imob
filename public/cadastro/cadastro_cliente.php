<?php

// Inclui os arquivos necessários
require_once __DIR__ . '/../../includes/auth.php';
require_once __DIR__ . '/../../includes/header.php';
require_once __DIR__ . '/../../includes/db.php';

$mensagem = '';
$mensagem2 = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Validação dos dados do formulário
    $cpf = trim($_POST['cpf']);
    $nome = trim($_POST['nome']);
    $email = trim($_POST['email']);
    $senha = password_hash($_POST['senha'], PASSWORD_DEFAULT); // Hash da senha
    $telefones = $_POST['telefones']; // Array de telefones

    // Conexão com o banco de dados
    $conn = connect_db();

    // Verificar se o CPF já existe
    $stmt = $conn->prepare("SELECT cpf FROM Cliente WHERE cpf = ?");
    $stmt->bind_param("i", $cpf);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        $mensagem2 = "CPF já cadastrado. Tente novamente.";
    } else {
        try {
            // Inicia a transação
            $conn->begin_transaction();

            // Inserir na tabela Pessoa
            $stmt = $conn->prepare("INSERT INTO Pessoa (id, nome, email, senha) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("isss", $cpf, $nome, $email, $senha);

            if (!$stmt->execute()) {
                throw new Exception("Erro ao cadastrar pessoa: " . $stmt->error);
            }

            // Inserir na tabela Cliente
            $stmt = $conn->prepare("INSERT INTO Cliente (cpf) VALUES (?)");
            $stmt->bind_param("i", $cpf);

            if (!$stmt->execute()) {
                throw new Exception("Erro ao cadastrar cliente: " . $stmt->error);
            }

            // Inserir na tabela Telefone
            $stmt = $conn->prepare("INSERT INTO Telefone (pessoa_id, telefone) VALUES (?, ?)");

            foreach ($telefones as $telefone) {
                // Verificar se o telefone não está vazio
                if (!empty($telefone)) {
                    $stmt->bind_param("is", $cpf, $telefone);

                    if (!$stmt->execute()) {
                        throw new Exception("Erro ao cadastrar telefone: " . $stmt->error);
                    }
                }
            }

            // Confirma a transação
            $conn->commit();
            $mensagem = "Novo cliente adicionado com sucesso";
        } catch (Exception $e) {
            // Em caso de erro, reverte a transação
            $conn->rollback();
            $mensagem = "Falha ao adicionar cliente: " . $mensagem2 . $e->getMessage();
        }

        // Fecha a declaração
        $stmt->close();
    }

    // Fecha a conexão
    $conn->close();
}
?>

<!-- Formulário de Registro de Cliente -->
<main>
    <h2>Cadastro Cliente</h2>
    <?php if ($mensagem): ?>
    <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
        <?php echo htmlspecialchars($mensagem); ?>
    </div>
    <?php endif; ?>

    <form action="cadastro_cliente.php" method="post">
        <label for="cpf">CPF:</label><br>
        <input type="number" id="cpf" name="cpf" placeholder="Digite seu CPF" required><br>
        <label for="nome">Nome:</label><br>
        <input type="text" id="nome" name="nome" placeholder="Digite seu nome" required><br>
        <label for="email">Email:</label><br>
        <input type="email" id="email" name="email" placeholder="Digite seu email" required><br>
        <label for="senha">Senha:</label><br>
        <input type="password" id="senha" name="senha" placeholder="Digite sua senha" required><br>
        <label for="telefones">Telefones:</label><br>
        <input type="tel" id="telefone1" name="telefones[]" placeholder="99 99999-9999"
                    pattern="[0-9]{2}[0-9]{5}[0-9]{4}" />
        <input type="tel" id="telefone2" name="telefones[]" placeholder="99 99999-9999"
                    pattern="[0-9]{2}[0-9]{5}[0-9]{4}" />

        <button type="submit">Registrar</button>
    </form>
</main>


<?php
require_once __DIR__ . '/../../includes/footer.php';
?>
