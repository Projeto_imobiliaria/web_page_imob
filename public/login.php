<?php
// public/login.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';

$mensagem = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = trim($_POST['email']);
    $senha = trim($_POST['senha']);

    if (empty($email) || empty($senha)) {
        $mensagem = "Email e senha são obrigatórios.";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $mensagem = "Email inválido.";
    } else {
        $conn = connect_db();

        // Verificar se é um corretor
        $stmt_corretor = $conn->prepare("SELECT p.id, p.senha FROM Corretor c INNER JOIN Pessoa p ON c.matricula = p.id WHERE p.email = ?");
        $stmt_corretor->bind_param("s", $email);
        $stmt_corretor->execute();
        $stmt_corretor->store_result();

        // Se for corretor
        if ($stmt_corretor->num_rows > 0) {
            $stmt_corretor->bind_result($id, $hash);
            $stmt_corretor->fetch();

            if (password_verify($senha, $hash)) {
                $_SESSION['user_id'] = $id;
                $_SESSION['user_type'] = 'corretor';
                header("Location: /public/index.php");
                exit;
            } else {
                $mensagem = "Senha incorreta.";
            }
        }

        // Verificar se é um cliente (se não for corretor)
        if (empty($_SESSION['user_id'])) {
            $stmt_cliente = $conn->prepare("SELECT p.id, p.senha FROM Cliente c INNER JOIN Pessoa p ON c.cpf = p.id WHERE p.email = ?");
            $stmt_cliente->bind_param("s", $email);
            $stmt_cliente->execute();
            $stmt_cliente->store_result();

            // Se for cliente
            if ($stmt_cliente->num_rows > 0) {
                $stmt_cliente->bind_result($id, $hash);
                $stmt_cliente->fetch();

                if (password_verify($senha, $hash)) {
                    $_SESSION['user_id'] = $id;
                    $_SESSION['user_type'] = 'cliente';
                    header("Location: /public/index.php");
                    exit;
                } else {
                    $mensagem = "Senha incorreta.";
                }
            } else {
                $mensagem = "Usuário não encontrado.";
            }
        }

        $stmt_corretor->close();
        $stmt_cliente->close();
        $conn->close();
    }
}
?>

<main>
    <h2>Login</h2>
    <?php if ($mensagem): ?>
        <div class="message error">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <form action="login.php" method="POST">
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" placeholder="Digite seu email" required>

        <label for="senha">Senha:</label>
        <input type="password" id="senha" name="senha" placeholder="Digite sua senha" required>

        <button type="submit">Login</button>
    </form>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
