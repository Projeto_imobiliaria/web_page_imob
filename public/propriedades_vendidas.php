<?php
// public/propriedades_vendidas.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$matricula = $_SESSION['user_id'];

$conn = connect_db();

$stmt = $conn->prepare("
    SELECT cc.data, p.titulo, p.descricao, p.preco, CONCAT(pessoa.nome, ' (', cl.cpf, ')') AS cliente_nome
    FROM ContratoCompra cc
    JOIN Propriedade p ON cc.propriedade_id = p.id
    JOIN Cliente cl ON cc.cliente_id = cl.cpf
    JOIN Pessoa pessoa ON cl.cpf = pessoa.id
    WHERE p.corretor_id = ?
");
$stmt->bind_param("i", $matricula);
$stmt->execute();
$stmt->bind_result($data, $titulo, $descricao, $preco, $cliente_nome);

$propriedades_vendidas = [];
while ($stmt->fetch()) {
    $propriedades_vendidas[] = [
        'data' => $data,
        'titulo' => $titulo,
        'descricao' => $descricao,
        'preco' => $preco,
        'cliente_nome' => $cliente_nome
    ];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Minhas Propriedades Vendidas</h2>
    <?php if ($propriedades_vendidas): ?>
        <table>
            <thead>
                <tr>
                    <th>Data da Venda</th>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Cliente</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propriedades_vendidas as $propriedade): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($propriedade['data']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['cliente_nome']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma propriedade vendida encontrada.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
