<?php
// public/perfil_corretor.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();


if(!is_logged_in()){
  header("Location: /public/login.php");
  exit;
}

if (is_cliente()) {
    header("Location: /public/login.php");
    exit;
}

$conn = connect_db();
$mensagem = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  // Verifique o token CSRF
  if (!isset($_POST['csrf_token']) || $_POST['csrf_token'] !== $_SESSION['csrf_token']) {
    $mensagem = "Falha na verificação do token CSRF.";
  }

  // Lógica para deletar telefone
  if (isset($_POST['remove_telefone'])) {
      $telefone = $_POST['remove_telefone'];
      $user_id = $_SESSION['user_id'];
      $stmt = $conn->prepare("DELETE FROM Telefone WHERE pessoa_id = ? AND telefone = ?");
      $stmt->bind_param("is", $user_id, $telefone);
      if ($stmt->execute()) {
          $mensagem = "Telefone removido com sucesso!";
      } else {
          $mensagem = "Erro ao remover o telefone: " . $stmt->error;
      }
      $stmt->close();
  }

  // Lógica para atualizar dados do corretor
  if (isset($_POST['nome']) && isset($_POST['email']) && isset($_POST['telefones'])) {
      $nome = trim($_POST['nome']);
      $email = trim($_POST['email']);
      $telefones = $_POST['telefones'];
      $corretor_id = $_SESSION['user_id'];

      if (!empty($nome) && !empty($email)) {
          $query = "UPDATE Pessoa SET nome = ?, email = ? WHERE id = ?";
          if (!empty($_POST['senha'])) {
              $senha = trim($_POST['senha']);
              $senha_hash = password_hash($senha, PASSWORD_BCRYPT);
              $query = "UPDATE Pessoa SET nome = ?, email = ?, senha = ? WHERE id = ?";
          }

          $stmt = $conn->prepare($query);
          if (!empty($_POST['senha'])) {
              $stmt->bind_param("sssi", $nome, $email, $senha_hash, $corretor_id);
          } else {
              $stmt->bind_param("ssi", $nome, $email, $corretor_id);
          }

          if ($stmt->execute()) {
              $_SESSION['user_nome'] = $nome;
              $_SESSION['user_email'] = $email;
              $mensagem = "Dados atualizados com sucesso!";
              $_SESSION['form_submitted'] = true; // Marca o formulário como submetido com sucesso
              // Regenerate the form token to prevent resubmission
              $_SESSION['form_token'] = bin2hex(random_bytes(32));
          } else {
              $mensagem = "Erro ao atualizar os dados: " . $stmt->error;
          }

          $stmt->close();

          // Atualizar telefones
          $stmt = $conn->prepare("INSERT INTO Telefone (pessoa_id, telefone) VALUES (?, ?)");
          foreach ($telefones as $tel) {
              if (!empty($tel)) {
                  $stmt->bind_param("is", $corretor_id, $tel);
                  $stmt->execute();
              }
          }
          $stmt->close();

      } else {
          $mensagem = "Nome e email são obrigatórios.";
      }
  }

  // Lógica para deletar a conta
  if (isset($_POST['delete_account']) && $_POST['delete_account'] === 'true') {
      $conn->begin_transaction();

      try {
          // Atualizar registros na tabela Compra para definir corretor_id como NULL
          $stmt = $conn->prepare("UPDATE Compra SET corretor_id = NULL WHERE corretor_id = ?");
          $stmt->bind_param("i", $corretor_id);
          $stmt->execute();
          $stmt->close();

          // Deletar registros na tabela Corretor
          $stmt = $conn->prepare("DELETE FROM Corretor WHERE matricula = ?");
          $stmt->bind_param("i", $corretor_id);
          $stmt->execute();
          $stmt->close();

          // Deletar registros na tabela Pessoa
          $stmt = $conn->prepare("DELETE FROM Pessoa WHERE id = ?");
          $stmt->bind_param("i", $corretor_id);
          $stmt->execute();
          $stmt->close();

          $conn->commit();

          session_destroy();
          header("Location: /public/index.php?mensagem=" . urlencode("Conta deletada com sucesso."));
          exit;
      } catch (mysqli_sql_exception $exception) {
          $conn->rollback();
          $mensagem = "Erro ao deletar a conta: " . $exception->getMessage();
      }
  }
}

// Busca dados do corretor
$corretor_id = $_SESSION['user_id'];
$stmt = $conn->prepare("SELECT c.matricula, c.cargo, p.nome, p.email, t.telefone FROM Pessoa p LEFT JOIN Telefone t ON p.id = t.pessoa_id LEFT JOIN Corretor c ON c.matricula = p.id WHERE p.id = ? GROUP BY t.telefone");
$stmt->bind_param("i", $corretor_id);
$stmt->execute();
$stmt->bind_result($matricula, $cargo, $nome, $email, $telefone);
$telefones = [];
while ($stmt->fetch()) {
    if (!empty($telefone)) {
        $telefones[] = $telefone;
    }
}
$stmt->close();
$conn->close();

?>

<main>

    <h2>Perfil do Corretor</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>

    <section>
        <h3>Dados Atuais</h3>
        <p><strong>Nome:</strong> <?php echo htmlspecialchars($nome) . '  <strong>Cargo:</strong> ' . htmlspecialchars($cargo); ?></p>
        <p><strong>Email:</strong> <?php echo htmlspecialchars($email); ?></p>
        <p><strong>Telefones:</strong><?php if (!$telefones) { echo ' Nenhum telefone cadastrado.'; } ?></p>
        <ul>
            <?php foreach ($telefones as $telefone): ?>
                <li>
                    <?php echo htmlspecialchars($telefone); ?>
                    <form action="perfil_corretor.php" method="POST" style="display:inline;">
                        <input type="hidden" name="remove_telefone" value="<?php echo htmlspecialchars($telefone); ?>">
                        <button type="submit">Remover</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section>
        <h3>Atualizar Dados</h3>
        <form action="perfil_corretor.php" method="POST">
            <label for="nome">Nome:</label>
            <input type="text" id="nome" name="nome" value="<?php echo htmlspecialchars($nome); ?>" required>

            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="<?php echo htmlspecialchars($email); ?>" required>

            <label for="telefones">Telefones:</label>
            <?php foreach ($telefones as $telefone): ?>
                <input type="hidden" name="telefones[]" value="<?php echo htmlspecialchars($telefone); ?>">
            <?php endforeach; ?>
            <div id="telefone-container">
                <?php foreach ($telefones as $telefone): ?>
                    <input type="tel" name="telefones[]" value="<?php echo htmlspecialchars($telefone); ?>" readonly>
                <?php endforeach; ?>
            </div>
            <button type="button" onclick="adicionarTelefone()">Adicionar Telefone</button>
            <div id="mensagem-telefone"></div>

            <label for="senha">Nova Senha (deixe em branco para manter a senha atual):</label>
            <input type="password" id="senha" name="senha">

            <button type="submit">Atualizar</button>
        </form>
        <!-- Botão para deletar a conta -->
        <form action="perfil_corretor.php" method="POST" onsubmit="return confirm('Você deseja deletar sua conta?.');">
            <input type="hidden" name="delete_account" value="true">
            <button type="submit" class="delete-button">Deletar Conta</button>
        </form>
    </section>
</main>

<script>
function adicionarTelefone() {
    var container = document.getElementById('telefone-container');
    var input = document.createElement('input');
    input.type = 'tel';
    input.name = 'telefones[]';
    input.placeholder = '99 99999-9999';
    input.pattern = '[0-9]{2}[0-9]{5}[0-9]{4}';
    container.appendChild(input);

    input.addEventListener('blur', function() {
        verificarTelefone(input);
    });
}

function verificarTelefone(input) {
    var telefone = input.value.trim();
    if (telefone === '') return;

    // Requisição AJAX para verificar telefone
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/public/verificar_telefone.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.telefone_existe) {
                input.setCustomValidity('Este telefone já está cadastrado.');
                document.getElementById('mensagem-telefone').textContent = 'Este telefone já está cadastrado.';
            } else {
                input.setCustomValidity('');
                document.getElementById('mensagem-telefone').textContent = '';
            }
        } else {
            console.error('Erro ao verificar o telefone.');
        }
    };
    xhr.send('telefone=' + encodeURIComponent(telefone));
}
</script>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
