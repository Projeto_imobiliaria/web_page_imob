<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';

$conn = connect_db();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['propriedade_id']) || !is_numeric($_POST['propriedade_id']) || !isset($_POST['cliente_id'])) {
        header("Location: /public/carrinho.php?mensagem=" . urlencode("Dados inválidos."));
        exit;
    }

    $propriedade_id = trim($_POST['propriedade_id']);
    $cliente_id = trim($_POST['cliente_id']);

    $stmt = $conn->prepare("DELETE FROM carrinho WHERE propriedade_id = ? AND cliente_id = ?");
    if (!$stmt) {
        header("Location: /public/carrinho.php?mensagem=" . urlencode("Erro na preparação da declaração SQL: " . $conn->error));
        $conn->close();
        exit;
    }

    $stmt->bind_param("is", $propriedade_id, $cliente_id);

    if ($stmt->execute()) {
        $mensagem = "Propriedade removida do carrinho com sucesso!";
    } else {
        $mensagem = "Erro ao remover a propriedade: " . $stmt->error;
    }

    $stmt->close();
    $conn->close();

    header("Location: /public/carrinho.php?mensagem=" . urlencode($mensagem));
    exit;
} else {
    header("Location: /public/carrinho.php?mensagem=" . urlencode("Método de requisição inválido."));
    exit;
}
?>
