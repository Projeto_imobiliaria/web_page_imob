<?php

// public/adicionar_carrinho.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$propriedade_id = $_GET['id'];
$cliente_id = $_SESSION['user_id'];

$conn = connect_db();

// Verifica se a propriedade já está no carrinho
$check_stmt = $conn->prepare("SELECT COUNT(*) FROM Carrinho WHERE cliente_id = ? AND propriedade_id = ?");
$check_stmt->bind_param("ii", $cliente_id, $propriedade_id);
$check_stmt->execute();
$check_stmt->bind_result($count);
$check_stmt->fetch();
$check_stmt->close();

if ($count > 0) {
    // Se a propriedade já estiver no carrinho, mostra uma mensagem de erro
    header("Location: /public/listar_propriedades_cliente.php?mensagem=" . urlencode("A propriedade já está no carrinho!"));
} else {
    // Se a propriedade não estiver no carrinho, adiciona-a
    $stmt = $conn->prepare("INSERT INTO Carrinho (cliente_id, propriedade_id) VALUES (?, ?)");
    $stmt->bind_param("ii", $cliente_id, $propriedade_id);

    if ($stmt->execute()) {
        header("Location: /public/listar_propriedades_cliente.php?mensagem=" . urlencode("Propriedade adicionada ao carrinho!"));
    } else {
        header("Location: /public/listar_propriedades_cliente.php?mensagem=" . urlencode("Erro ao adicionar ao carrinho: " . $stmt->error));
    }

    $stmt->close();
}

$conn->close();
?>
