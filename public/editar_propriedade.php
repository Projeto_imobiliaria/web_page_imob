<?php
// public/editar_propriedade.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_corretor()) {
    header("Location: /public/index.php");
    exit;
}

$conn = connect_db();
$mensagem = '';
$propriedade_id = $_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $titulo = trim($_POST['titulo']);
    $descricao = trim($_POST['descricao']);
    $preco = trim($_POST['preco']);

    if (!empty($titulo) && !empty($descricao) && !empty($preco)) {
        $stmt = $conn->prepare("UPDATE propriedades SET titulo = ?, descricao = ?, preco = ? WHERE id = ?");
        $stmt->bind_param("ssdi", $titulo, $descricao, $preco, $propriedade_id);

        if ($stmt->execute()) {
            $mensagem = "Propriedade atualizada com sucesso!";
        } else {
            $mensagem = "Erro ao atualizar a propriedade: " . $stmt->error;
        }

        $stmt->close();
    } else {
        $mensagem = "Todos os campos são obrigatórios.";
    }
} else {
    $stmt = $conn->prepare("SELECT titulo, descricao, preco FROM propriedades WHERE id = ?");
    $stmt->bind_param("i", $propriedade_id);
    $stmt->execute();
    $stmt->bind_result($titulo, $descricao, $preco);
    $stmt->fetch();
    $stmt->close();
}

$conn->close();
?>

<main>
    <h2>Editar Propriedade</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <form action="editar_propriedade.php?id=<?php echo $propriedade_id; ?>" method="POST">
        <label for="titulo">Título:</label>
        <input type="text" id="titulo" name="titulo" value="<?php echo htmlspecialchars($titulo); ?>" required>

        <label for="descricao">Descrição:</label>
        <textarea id="descricao" name="descricao" required><?php echo htmlspecialchars($descricao); ?></textarea>

        <label for="preco">Preço:</label>
        <input type="number" id="preco" name="preco" value="<?php echo htmlspecialchars($preco); ?>" required>

        <button type="submit">Atualizar</button>
    </form>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
