<?php
// public/verificar_telefone.php
require_once __DIR__ . '/../includes/db.php';

// Função para verificar se o telefone existe
function verificarTelefone($telefone) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT COUNT(*) FROM Telefone WHERE telefone = ?");
    $stmt->bind_param("s", $telefone);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->fetch();
    $stmt->close();
    $conn->close();

    return $count > 0;
}

// Verificar se o POST tem a variável telefone
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['telefone'])) {
    $telefone = trim($_POST['telefone']);
    $telefoneExiste = verificarTelefone($telefone);

    // Retornar resposta JSON
    header('Content-Type: application/json');
    echo json_encode(['telefone_existe' => $telefoneExiste]);
    exit;
}

// Se não houver telefone no POST, retornar erro
http_response_code(400);
echo json_encode(['error' => 'Telefone não fornecido']);
?>
