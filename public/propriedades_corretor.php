<?php
// public/propriedades_corretor.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';

check_login();

if (!is_corretor()) {
    header("Location: /public/index.php");
    exit;
}

$matricula = $_SESSION['user_id'];

$conn = connect_db();

$stmt = $conn->prepare("
    SELECT id, titulo, descricao, preco
    FROM Propriedade
    WHERE corretor_matricula = ?
");
$stmt->bind_param("i", $matricula);
$stmt->execute();
$stmt->bind_result($id, $titulo, $descricao, $preco);

$propriedades = [];
while ($stmt->fetch()) {
    $propriedades[] = [
        'id' => $id,
        'titulo' => $titulo,
        'descricao' => $descricao,
        'preco' => $preco
    ];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Minhas Propriedades</h2>
    <a href="adicionar_propriedade.php">Adicionar Propriedade</a>
    <?php if ($propriedades): ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propriedades as $propriedade): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                        <td>
                            <a href="editar_propriedade.php?id=<?php echo $propriedade['id']; ?>">Editar</a>
                            <form action="remover_propriedade.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $propriedade['id']; ?>">
                                <button type="submit" onclick="return confirm('Tem certeza que deseja remover esta propriedade?');">Remover</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma propriedade encontrada.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
