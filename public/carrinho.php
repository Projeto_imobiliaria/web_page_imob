<?php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

$cliente_id = $_SESSION['user_id'];

if (isset($_GET['mensagem'])) {
    $mensagem = urldecode($_GET['mensagem']);
    echo "<p>$mensagem</p>";
}

$conn = connect_db();

$stmt = $conn->prepare("SELECT p.id, p.titulo, p.descricao, p.preco FROM Carrinho c JOIN Propriedade p ON c.propriedade_id = p.id WHERE c.cliente_id = ?");
$stmt->bind_param("i", $cliente_id);
$stmt->execute();
$stmt->bind_result($id, $titulo, $descricao, $preco);

$carrinho = [];
while ($stmt->fetch()) {
    $carrinho[] = ['id' => $id, 'titulo' => $titulo, 'descricao' => $descricao, 'preco' => $preco];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Meu Carrinho</h2>
    <?php if ($carrinho): ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($carrinho as $item): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($item['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($item['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($item['preco']); ?></td>
                        <td>
                            <form action="remover_do_carrinho.php" method="post" style="display:inline;">
                                <input type="hidden" name="propriedade_id" value="<?php echo $item['id']; ?>">
                                <button type="submit" onclick="return confirm('Tem certeza que deseja remover esta propriedade?');">Remover</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhum item encontrado no carrinho.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
