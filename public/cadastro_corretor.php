<?php
// public/cadastro_corretor.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

//if (!is_corretor()) { // Supondo que apenas um admin possa cadastrar novos corretores
//    header("Location: /public/index.php");
//    exit;
//}

//if (!is_admin()) { // Supondo que apenas um admin possa cadastrar novos corretores
//    header("Location: /public/index.php");
//    exit;
//}

$mensagem = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nome = trim($_POST['nome']);
    $email = trim($_POST['email']);
    $telefone = trim($_POST['telefone']);
    $senha = trim($_POST['senha']);
    $tipo = $_POST['tipo'];

    if (!empty($nome) && !empty($email) && !empty($telefone) && !empty($senha) && !empty($tipo)) {
        $senha_hash = password_hash($senha, PASSWORD_BCRYPT);
        $conn = connect_db();
        $stmt = $conn->prepare("INSERT INTO corretores (nome, email, telefone, senha, tipo) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", $nome, $email, $telefone, $senha_hash, $tipo);

        if ($stmt->execute()) {
            $mensagem = "Corretor cadastrado com sucesso!";
        } else {
            $mensagem = "Erro ao cadastrar o corretor: " . $stmt->error;
        }

        $stmt->close();
        $conn->close();
    } else {
        $mensagem = "Todos os campos são obrigatórios.";
    }
}
?>

<main>
    <h2>Cadastrar Corretor</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <form action="cadastro_corretor.php" method="POST">
        <label for="nome">Nome:</label>
        <input type="text" id="nome" name="nome" required>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>

        <label for="telefone">Telefone:</label>
        <input type="text" id="telefone" name="telefone" required>

        <label for="senha">Senha:</label>
        <input type="password" id="senha" name="senha" required>

        <label for="tipo">Tipo:</label>
        <select id="tipo" name="tipo" required>
            <option value="corretor">Corretor</option>
            <option value="gerente">Gerente</option>
        </select>

        <button type="submit">Cadastrar</button>
    </form>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
