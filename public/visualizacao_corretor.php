<?php
// public/visualizacao_corretor.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

$conn = connect_db();

$result = $conn->query("SELECT * FROM corretores");

?>

<main>
    <h2>Corretores Cadastrados</h2>
    <table>
        <thead>
            <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Telefone</th>
            </tr>
        </thead>
        <tbody>
            <?php while ($corretor = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo htmlspecialchars($corretor['nome']); ?></td>
                    <td><?php echo htmlspecialchars($corretor['email']); ?></td>
                    <td><?php echo htmlspecialchars($corretor['telefone']); ?></td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
</main>

<?php
$conn->close();
require_once __DIR__ . '/../includes/footer.php';
?>
