<?php

// public/listar_propriedades_cliente.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$conn = connect_db();
$stmt = $conn->prepare("SELECT id, titulo, descricao, preco FROM Propriedade WHERE status = 'disponivel'");
$stmt->execute();
$stmt->bind_result($id, $titulo, $descricao, $preco);

$propriedades = [];
while ($stmt->fetch()) {
    $propriedades[] = ['id' => $id, 'titulo' => $titulo, 'descricao' => $descricao, 'preco' => $preco];
}

$stmt->close();
$conn->close();
?>

<main>
    <h2>Propriedades Disponíveis</h2>
    <?php if ($propriedades): ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propriedades as $propriedade): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                        <td>
                            <a href="adicionar_carrinho.php?id=<?php echo $propriedade['id']; ?>">Adicionar ao Carrinho</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma propriedade disponível.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
