<?php
// public/visualizar_carrinhos.php

require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';


$conn = connect_db();
$mensagem = '';

// Verificar se uma compra foi aprovada
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['carrinho_id'])) {
    $carrinho_id = $_POST['carrinho_id'];

    // Obter a propriedade do carrinho
    $stmt = $conn->prepare("
        SELECT
            Carrinho.cliente_id,
            Carrinho.propriedade_id,
            Propriedade.corretor_id
        FROM Carrinho
        JOIN Propriedade ON Carrinho.propriedade_id = Propriedade.id
        WHERE Carrinho.id = ?
    ");
    $stmt->bind_param("i", $carrinho_id);
    $stmt->execute();
    $stmt->bind_result($cliente_id, $propriedade_id, $corretor_id);
    $stmt->fetch();
    $stmt->close();

    // Inserir a compra na tabela de compras
    $stmt = $conn->prepare("INSERT INTO Compra (cliente_id, propriedade_id, corretor_id, data_compra, status) VALUES (?, ?, ?, NOW(), 'aprovada')");
    $stmt->bind_param("iii", $cliente_id, $propriedade_id, $corretor_id);

    if ($stmt->execute()) {
        // Remover a propriedade do carrinho
        $stmt = $conn->prepare("DELETE FROM Carrinho WHERE id = ?");
        $stmt->bind_param("i", $carrinho_id);
        $stmt->execute();
        $stmt->close();

        $mensagem = "Compra aprovada com sucesso!";
    } else {
        $mensagem = "Erro ao aprovar a compra: " . $stmt->error;
    }
}

// Obter informações dos carrinhos
$query = "
    SELECT
        Cliente.cpf,
        Pessoa.nome AS cliente_nome,
        Propriedade.id AS propriedade_id,
        Propriedade.titulo,
        Propriedade.descricao,
        Propriedade.preco,
        Carrinho.id AS carrinho_id
    FROM
        Carrinho
    INNER JOIN Cliente ON Carrinho.cliente_id = Cliente.cpf
    INNER JOIN Pessoa ON Cliente.cpf = Pessoa.id
    INNER JOIN Propriedade ON Carrinho.propriedade_id = Propriedade.id
    ORDER BY Cliente.cpf
";

$result = $conn->query($query);

$clientes_carrinhos = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $clientes_carrinhos[$row['cpf']]['cliente_nome'] = $row['cliente_nome'];
        $clientes_carrinhos[$row['cpf']]['propriedades'][] = [
            'id' => $row['propriedade_id'],
            'titulo' => $row['titulo'],
            'descricao' => $row['descricao'],
            'preco' => $row['preco'],
            'carrinho_id' => $row['carrinho_id']
        ];
    }
} else {
    $mensagem = "Nenhum carrinho encontrado.";
}

$conn->close();
?>

<main>
    <h2>Carrinhos dos Clientes</h2>
    <?php if ($mensagem): ?>
        <div class="message">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($clientes_carrinhos)): ?>
        <?php foreach ($clientes_carrinhos as $cpf => $dados): ?>
            <section>
                <h3>Cliente: <?php echo htmlspecialchars($dados['cliente_nome']); ?> (CPF: <?php echo htmlspecialchars($cpf); ?>)</h3>
                <ul>
                    <?php foreach ($dados['propriedades'] as $propriedade): ?>
                        <li>
                            <strong>Título:</strong> <?php echo htmlspecialchars($propriedade['titulo']); ?><br>
                            <strong>Descrição:</strong> <?php echo htmlspecialchars($propriedade['descricao']); ?><br>
                            <strong>Preço:</strong> R$ <?php echo number_format($propriedade['preco'], 2, ',', '.'); ?><br>
                            <form action="visualizar_carrinhos.php" method="POST" style="display:inline;">
                                <input type="hidden" name="carrinho_id" value="<?php echo $propriedade['carrinho_id']; ?>">
                                <button type="submit">Aprovar Compra</button>
                            </form>
                            <form action="remover_do_carrinho.php" method="POST" style="display:inline;">
                                <input type="hidden" name="carrinho_id" value="<?php echo $propriedade['carrinho_id']; ?>">
                                <button type="submit">Remover</button>
                            </form>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </section>
        <?php endforeach; ?>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
