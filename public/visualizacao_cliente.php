<?php
// public/visualizacao_cliente.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$conn = connect_db();

$result = $conn->query("SELECT * FROM propriedades");

?>

<main>
    <h2>Propriedades Disponíveis</h2>
    <table>
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Endereço</th>
                <th>Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php while ($propriedade = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                    <td><?php echo htmlspecialchars($propriedade['endereco']); ?></td>
                    <td><?php echo htmlspecialchars($propriedade['valor']); ?></td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
</main>

<?php
$conn->close();
require_once __DIR__ . '/../includes/footer.php';
?>
