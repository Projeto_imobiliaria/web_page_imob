<?php
// public/remover_propriedade.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/db.php';

check_login();

if (!is_cliente() || !is_gerente() || !is_corretor()) {
    header("Location: /public/index.php");
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $propriedade_id = trim($_POST['id']);
    $cpf = $_SESSION['user_id'];

    $conn = connect_db();

    $stmt = $conn->prepare("DELETE FROM CarrinhoInteresse WHERE cliente_cpf = ? AND propriedade_id = ?");
    $stmt->bind_param("si", $cpf, $propriedade_id);

    if ($stmt->execute()) {
        $mensagem = "Propriedade removida com sucesso do carrinho de interesses!";
    } else {
        $mensagem = "Erro ao remover a propriedade do carrinho de interesses: " . $stmt->error;
    }

    $stmt->close();
    $conn->close();

    header("Location: /public/carrinho.php?mensagem=" . urlencode($mensagem));
    exit;
} else {
    header("Location: /public/index.php");
    exit;
}
?>
