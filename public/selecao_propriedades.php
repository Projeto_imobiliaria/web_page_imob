<?php
// public/selecao_propriedades.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();
if (!is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$conn = connect_db();
$mensagem = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cliente_id = $_SESSION['user_id'];
    $propriedades = $_POST['propriedades'];

    if (empty($propriedades)) {
        $mensagem = "Propriedades são obrigatórias.";
    } else {
        foreach ($propriedades as $propriedade_id) {
            $stmt = $conn->prepare("INSERT INTO selecoes (cliente_id, propriedade_id) VALUES (?, ?)");
            $stmt->bind_param("ii", $cliente_id, $propriedade_id);

            if (!$stmt->execute()) {
                $mensagem = "Erro ao selecionar propriedades: " . $stmt->error;
                break;
            }
            $stmt->close();
        }

        if (empty($mensagem)) {
            $mensagem = "Propriedades selecionadas com sucesso!";
        }
    }
}

$propriedades = $conn->query("SELECT id, descricao FROM propriedades");

?>

<main>
    <h2>Seleção de Propriedades</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <form action="selecao_propriedades.php" method="POST">
        <label for="propriedades">Propriedades:</label>
        <select id="propriedades" name="propriedades[]" multiple required>
            <?php while ($propriedade = $propriedades->fetch_assoc()): ?>
                <option value="<?php echo $propriedade['id']; ?>"><?php echo htmlspecialchars($propriedade['descricao']); ?></option>
            <?php endwhile; ?>
        </select>

        <button type="submit">Selecionar</button>
    </form>
</main>

<?php
$conn->close();
require_once __DIR__ . '/../includes/footer.php';
?>
