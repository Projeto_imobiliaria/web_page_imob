<?php

// public/adicionar_propriedade.php
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if (!is_corretor()) {
    header("Location: /public/index.php");
    exit;
}

$mensagem = '';

// Verificação e recuperação do corretor_id da sessão
$corretor_id = $_SESSION['user_id'] ?? null;

if (!$corretor_id) {
    $mensagem = "Erro: ID do corretor não encontrado.";
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $titulo = trim($_POST['titulo']);
    $descricao = trim($_POST['descricao']);
    $preco = trim($_POST['preco']);
    $tipo_propriedade = $_POST['tipo_propriedade'];

    if (!empty($titulo) && !empty($descricao) && !empty($preco) && !empty($tipo_propriedade)) {
        $conn = connect_db();
        $conn->begin_transaction();

        try {
            // Inserir na tabela Propriedade
            $stmt = $conn->prepare("INSERT INTO Propriedade (titulo, descricao, preco, corretor_id) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssdi", $titulo, $descricao, $preco, $corretor_id);
            $stmt->execute();
            $propriedade_id = $conn->insert_id;

            if ($tipo_propriedade == 'Terreno') {
                $tipo_terreno = $_POST['tipo_terreno'];
                $stmt = $conn->prepare("INSERT INTO Terreno (id, tipo) VALUES (?, ?)");
                $stmt->bind_param("is", $propriedade_id, $tipo_terreno);
            } elseif ($tipo_propriedade == 'Imovel') {
                $tipo_imovel = $_POST['tipo_imovel'];
                $qtd_quartos = $_POST['qtd_quartos'];
                $qtd_banheiros = $_POST['qtd_banheiros'];
                $qtd_suites = $_POST['qtd_suites'];
                $qtd_vagas_garagem = $_POST['qtd_vagas_garagem'];
                $stmt = $conn->prepare("INSERT INTO Imovel (id, tipo, qtd_quartos, qtd_banheiros, qtd_suites, qtd_vagas_garagem) VALUES (?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("isiiii", $propriedade_id, $tipo_imovel, $qtd_quartos, $qtd_banheiros, $qtd_suites, $qtd_vagas_garagem);
            }

            $stmt->execute();
            $conn->commit();
            $mensagem = "Propriedade adicionada com sucesso!";
        } catch (Exception $e) {
            $conn->rollback();
            $mensagem = "Erro ao adicionar a propriedade: " . $e->getMessage();
        }

        $stmt->close();
        $conn->close();
    } else {
        $mensagem = "Todos os campos são obrigatórios.";
    }
}
?>

<main>
    <h2>Adicionar Propriedade</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>
    <form action="adicionar_propriedade.php" method="POST">
        <label for="titulo">Título:</label>
        <input type="text" id="titulo" name="titulo" required>

        <label for="descricao">Descrição:</label>
        <textarea id="descricao" name="descricao" required></textarea>

        <label for="preco">Preço:</label>
        <input type="number" step="0.01" id="preco" name="preco" required>

        <label for="tipo_propriedade">Tipo de Propriedade:</label>
        <select id="tipo_propriedade" name="tipo_propriedade" required>
            <option value="">Selecione</option>
            <option value="Terreno">Terreno</option>
            <option value="Imovel">Imóvel</option>
        </select>

        <div id="terreno_fields" style="display: none;">
            <label for="tipo_terreno">Tipo de Terreno:</label>
            <select id="tipo_terreno" name="tipo_terreno">
                <option value="Rural">Rural</option>
                <option value="Urbano">Urbano</option>
            </select>
        </div>

        <div id="imovel_fields" style="display: none;">
            <label for="tipo_imovel">Tipo de Imóvel:</label>
            <select id="tipo_imovel" name="tipo_imovel">
                <option value="Apartamento">Apartamento</option>
                <option value="Casa">Casa</option>
                <option value="Chácara">Chácara</option>
                <option value="Outro">Outro</option>
            </select>

            <label for="qtd_quartos">Quantidade de Quartos:</label>
            <input type="number" id="qtd_quartos" name="qtd_quartos">

            <label for="qtd_banheiros">Quantidade de Banheiros:</label>
            <input type="number" id="qtd_banheiros" name="qtd_banheiros">

            <label for="qtd_suites">Quantidade de Suítes:</label>
            <input type="number" id="qtd_suites" name="qtd_suites">

            <label for="qtd_vagas_garagem">Quantidade de Vagas na Garagem:</label>
            <input type="number" id="qtd_vagas_garagem" name="qtd_vagas_garagem">
        </div>

        <button type="submit">Adicionar</button>
    </form>
</main>

<script>
document.getElementById('tipo_propriedade').addEventListener('change', function() {
    var tipo = this.value;
    document.getElementById('terreno_fields').style.display = (tipo === 'Terreno') ? 'block' : 'none';
    document.getElementById('imovel_fields').style.display = (tipo === 'Imovel') ? 'block' : 'none';
});
</script>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
