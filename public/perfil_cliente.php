<?php
// public/perfil_cliente.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';
check_login();

if(!is_logged_in()){
  header("Location: /public/login.php");
  exit;
}

if (!is_cliente()) {
    header("Location: /public/index.php");
    exit;
}

$mensagem = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = connect_db();
    $cliente_id = $_SESSION['user_id'];

    if (isset($_POST['remove_telefone'])) {
        $telefone = trim($_POST['remove_telefone']);
        $stmt = $conn->prepare("DELETE FROM Telefone WHERE pessoa_id = ? AND telefone = ?");
        $stmt->bind_param("is", $cliente_id, $telefone);

        if ($stmt->execute()) {
            $mensagem = "Telefone removido com sucesso!";
        } else {
            $mensagem = "Erro ao remover telefone: " . $stmt->error;
        }

        $stmt->close();
    } else {
        $nome = trim($_POST['nome']);
        $email = trim($_POST['email']);
        $telefones = $_POST['telefones']; // Array de telefones

        if (!empty($nome) && !empty($email) && !empty($telefones)) {
            $conn->begin_transaction();

            try {
                // Atualizar dados na tabela Pessoa
                $stmt = $conn->prepare("UPDATE Pessoa SET nome = ?, email = ? WHERE id = ?");
                $stmt->bind_param("ssi", $nome, $email, $cliente_id);
                $stmt->execute();

                // Atualizar telefones
                $stmt = $conn->prepare("DELETE FROM Telefone WHERE pessoa_id = ?");
                $stmt->bind_param("i", $cliente_id);
                $stmt->execute();

                $stmt = $conn->prepare("INSERT INTO Telefone (pessoa_id, telefone) VALUES (?, ?)");
                foreach ($telefones as $telefone) {
                    if (!empty($telefone)) {
                        $stmt->bind_param("is", $cliente_id, $telefone);
                        $stmt->execute();
                    }
                }

                if (!empty($_POST['senha'])) {
                    $senha = trim($_POST['senha']);
                    $senha_hash = password_hash($senha, PASSWORD_BCRYPT);
                    $stmt = $conn->prepare("UPDATE Pessoa SET senha = ? WHERE id = ?");
                    $stmt->bind_param("si", $senha_hash, $cliente_id);
                    $stmt->execute();
                }

                $conn->commit();
                $mensagem = "Dados atualizados com sucesso!";
            } catch (Exception $e) {
                $conn->rollback();
                $mensagem = "Erro ao atualizar os dados: " . $e->getMessage();
            }

            $stmt->close();
            $conn->close();

            // Atualizar dados da sessão
            $_SESSION['user_nome'] = $nome;
            $_SESSION['user_email'] = $email;
        } else {
            $mensagem = "Todos os campos são obrigatórios.";
        }
    }
}

$cliente_id = $_SESSION['user_id'];
$conn = connect_db();
$stmt = $conn->prepare("SELECT nome, email FROM Pessoa WHERE id = ?");
$stmt->bind_param("i", $cliente_id);
$stmt->execute();
$stmt->bind_result($nome, $email);
$stmt->fetch();
$stmt->close();

// Obter telefones
$stmt = $conn->prepare("SELECT telefone FROM Telefone WHERE pessoa_id = ?");
$stmt->bind_param("i", $cliente_id);
$stmt->execute();
$result = $stmt->get_result();
$telefones = [];
while ($row = $result->fetch_assoc()) {
    $telefones[] = $row['telefone'];
}
$stmt->close();
$conn->close();
?>

<main>
    <h2>Perfil do Cliente</h2>
    <?php if ($mensagem): ?>
        <div class="message <?php echo strpos($mensagem, 'sucesso') !== false ? 'success' : 'error'; ?>">
            <?php echo htmlspecialchars($mensagem); ?>
        </div>
    <?php endif; ?>

    <section>
        <h3>Dados Atuais</h3>
        <p><strong>Nome:</strong> <?php echo htmlspecialchars($nome); ?></p>
        <p><strong>Email:</strong> <?php echo htmlspecialchars($email); ?></p>
        <p><strong>Telefones:</strong><?php if (!$telefones){echo ' Nenhum telefone cadastrado.'; }?></p>

        <ul>
            <?php foreach ($telefones as $telefone): ?>
                <li>
                    <?php echo htmlspecialchars($telefone); ?>
                    <form action="perfil_cliente.php" method="POST" style="display:inline;">
                        <input type="hidden" name="remove_telefone" value="<?php echo htmlspecialchars($telefone); ?>">
                        <button type="submit">Remover</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>

    </section>

    <section>
        <h3>Atualizar Dados</h3>
        <form action="perfil_cliente.php" method="POST">
            <label for="nome">Nome:</label>
            <input type="text" id="nome" name="nome" value="<?php echo htmlspecialchars($nome); ?>" required>

            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="<?php echo htmlspecialchars($email); ?>" required>

            <label for="telefones">Telefones:</label>
            <?php foreach ($telefones as $telefone): ?>
                <input type="hidden" name="telefones[]" value="<?php echo htmlspecialchars($telefone); ?>">
            <?php endforeach; ?>
            <input type="tel" name="telefones[]" placeholder="99 99999-9999"
                        pattern="[0-9]{2}[0-9]{5}[0-9]{4}" />

            <label for="senha">Nova Senha (deixe em branco para manter a senha atual):</label>
            <input type="password" id="senha" name="senha">

            <button type="submit">Atualizar</button>
        </form>
    </section>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
