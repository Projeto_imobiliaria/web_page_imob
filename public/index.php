<?php
// public/index.php

require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';

$conn = connect_db();

// Inicializar filtros
$tipo_propriedade = isset($_GET['tipo_propriedade']) ? $_GET['tipo_propriedade'] : '';
$qtd_quartos = isset($_GET['qtd_quartos']) ? $_GET['qtd_quartos'] : '';
$qtd_banheiros = isset($_GET['qtd_banheiros']) ? $_GET['qtd_banheiros'] : '';
$qtd_suites = isset($_GET['qtd_suites']) ? $_GET['qtd_suites'] : '';
$qtd_vagas_garagem = isset($_GET['qtd_vagas_garagem']) ? $_GET['qtd_vagas_garagem'] : '';

// Consulta SQL com filtros
$query = "
    SELECT p.id, p.titulo, p.descricao, p.preco, p.status, pe.nome AS corretor_nome,
           CASE
               WHEN t.tipo IS NOT NULL THEN 'terreno'
               WHEN i.tipo IS NOT NULL THEN 'imovel'
           END AS tipo_propriedade,
           COALESCE(t.tipo, i.tipo) AS tipo,
           i.qtd_quartos, i.qtd_banheiros, i.qtd_suites, i.qtd_vagas_garagem
    FROM Propriedade p
    LEFT JOIN Corretor c ON p.corretor_id = c.matricula
    LEFT JOIN Pessoa pe ON c.matricula = pe.id
    LEFT JOIN Terreno t ON p.id = t.id
    LEFT JOIN Imovel i ON p.id = i.id
    WHERE 1=1
";

if ($tipo_propriedade) {
    if ($tipo_propriedade == 'terreno') {
        $query .= " AND t.tipo IS NOT NULL";
    } elseif ($tipo_propriedade == 'imovel') {
        $query .= " AND i.tipo IS NOT NULL";
        if ($qtd_quartos) {
            $query .= " AND i.qtd_quartos = " . intval($qtd_quartos);
        }
        if ($qtd_banheiros) {
            $query .= " AND i.qtd_banheiros = " . intval($qtd_banheiros);
        }
        if ($qtd_suites) {
            $query .= " AND i.qtd_suites = " . intval($qtd_suites);
        }
        if ($qtd_vagas_garagem) {
            $query .= " AND i.qtd_vagas_garagem = " . intval($qtd_vagas_garagem);
        }
    }
}

$result = $conn->query($query);

$propriedades = [];
while ($row = $result->fetch_assoc()) {
    $propriedades[] = $row;
}

$conn->close();
?>

<main>

    <h2>Propriedades</h2>
    <form method="get" action="/public/index.php">
        <label for="tipo_propriedade">Tipo de Propriedade:</label>
        <select id="tipo_propriedade" name="tipo_propriedade">
            <option value="">Todos</option>
            <option value="terreno" <?php if ($tipo_propriedade == 'terreno') echo 'selected'; ?>>Terreno</option>
            <option value="imovel" <?php if ($tipo_propriedade == 'imovel') echo 'selected'; ?>>Imóvel</option>
        </select>

        <div id="filtros_imovel" style="display: <?php echo ($tipo_propriedade == 'imovel') ? 'block' : 'none'; ?>;">
            <label for="qtd_quartos">Quartos:</label>
            <select id="qtd_quartos" name="qtd_quartos">
                <option value="">Todos</option>
                <?php for ($i = 1; $i <= 10; $i++): ?>
                    <option value="<?php echo $i; ?>" <?php if ($qtd_quartos == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>

            <label for="qtd_banheiros">Banheiros:</label>
            <select id="qtd_banheiros" name="qtd_banheiros">
                <option value="">Todos</option>
                <?php for ($i = 1; $i <= 10; $i++): ?>
                    <option value="<?php echo $i; ?>" <?php if ($qtd_banheiros == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>

            <label for="qtd_suites">Suítes:</label>
            <select id="qtd_suites" name="qtd_suites">
                <option value="">Todos</option>
                <?php for ($i = 1; $i <= 10; $i++): ?>
                    <option value="<?php echo $i; ?>" <?php if ($qtd_suites == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>

            <label for="qtd_vagas_garagem">Vagas na Garagem:</label>
            <select id="qtd_vagas_garagem" name="qtd_vagas_garagem">
                <option value="">Todos</option>
                <?php for ($i = 0; $i <= 10; $i++): ?>
                    <option value="<?php echo $i; ?>" <?php if ($qtd_vagas_garagem == $i) echo 'selected'; ?>><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>
        </div>

        <button type="submit">Filtrar</button>
    </form>

    <?php if ($propriedades): ?>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Status</th>
                    <th>Corretor</th>
                    <th>Tipo de Propriedade</th>
                    <th>Tipo</th>
                    <th>Quartos</th>
                    <th>Banheiros</th>
                    <th>Suítes</th>
                    <th>Vagas na Garagem</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propriedades as $propriedade): ?>
                    <tr>
                        <td><?php echo htmlspecialchars($propriedade['id']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['status']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['corretor_nome']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['tipo_propriedade']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['tipo']); ?></td>

                        <?php if ($propriedade['tipo_propriedade'] === 'imovel'): ?>
                        <td><?php echo htmlspecialchars($propriedade['qtd_quartos']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['qtd_banheiros']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['qtd_suites']); ?></td>
                        <td><?php echo htmlspecialchars($propriedade['qtd_vagas_garagem']); ?></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Nenhuma propriedade encontrada.</p>
    <?php endif; ?>
</main>

<?php
require_once __DIR__ . '/../includes/footer.php';
?>
<script>
    document.getElementById('tipo_propriedade').addEventListener('change', function() {
        var display = this.value === 'imovel' ? 'block' : 'none';
        document.getElementById('filtros_imovel').style.display = display;
    });
</script>
