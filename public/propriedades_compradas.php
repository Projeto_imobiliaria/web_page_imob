<?php
// public/propriedades_compradas.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';

$cliente_id = $_SESSION['user_id'];

$conn = connect_db();
$stmt = $conn->prepare("
    SELECT p.id, p.titulo, p.descricao, p.preco,
           IF(t.id IS NOT NULL, 'Terreno', 'Imovel') AS tipo_propriedade,
           IFNULL(t.tipo, i.tipo) AS tipo,
           i.qtd_quartos, i.qtd_banheiros, i.qtd_suites, i.qtd_vagas_garagem,
           c.data_compra, pessoa.nome AS corretor_nome
    FROM Propriedade p
    LEFT JOIN Terreno t ON p.id = t.id
    LEFT JOIN Imovel i ON p.id = i.id
    JOIN Compra c ON p.id = c.propriedade_id
    JOIN Cliente cli ON c.cliente_id = cli.cpf
    JOIN Pessoa pessoa ON p.corretor_id = pessoa.id
    WHERE c.cliente_id = ? AND c.status = 'aprovada'
");
$stmt->bind_param("i", $cliente_id);
$stmt->execute();
$stmt->bind_result($id, $titulo, $descricao, $preco, $tipo_propriedade, $tipo, $qtd_quartos, $qtd_banheiros, $qtd_suites, $qtd_vagas_garagem, $data_compra, $corretor_nome);

$propriedades_compradas = [];
while ($stmt->fetch()) {
    $propriedades_compradas[] = [
        'id' => $id,
        'titulo' => $titulo,
        'descricao' => $descricao,
        'preco' => $preco,
        'tipo_propriedade' => $tipo_propriedade,
        'tipo' => $tipo,
        'qtd_quartos' => $qtd_quartos,
        'qtd_banheiros' => $qtd_banheiros,
        'qtd_suites' => $qtd_suites,
        'qtd_vagas_garagem' => $qtd_vagas_garagem,
        'data_compra' => $data_compra,
        'corretor_nome' => $corretor_nome,
    ];
}

$stmt->close();
$conn->close();
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Propriedades Compradas</title>
    <link rel="stylesheet" href="/public/css/styles.css">
</head>
<body>
    <?php require_once __DIR__ . '/../includes/header.php'; ?>
    <main>
        <h2>Propriedades Compradas</h2>
        <?php if ($propriedades_compradas): ?>
            <table>
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Descrição</th>
                        <th>Preço</th>
                        <th>Tipo</th>
                        <th>Data da Compra</th>
                        <th>Corretor</th>
                        <?php if (in_array('Imovel', array_column($propriedades_compradas, 'tipo_propriedade'))): ?>
                            <th>Quartos</th>
                            <th>Banheiros</th>
                            <th>Suítes</th>
                            <th>Vagas na Garagem</th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($propriedades_compradas as $propriedade): ?>
                        <tr>
                            <td><?php echo htmlspecialchars($propriedade['titulo']); ?></td>
                            <td><?php echo htmlspecialchars($propriedade['descricao']); ?></td>
                            <td><?php echo htmlspecialchars($propriedade['preco']); ?></td>
                            <td><?php echo htmlspecialchars($propriedade['tipo']); ?></td>
                            <td><?php echo htmlspecialchars($propriedade['data_compra']); ?></td>
                            <td><?php echo htmlspecialchars($propriedade['corretor_nome']); ?></td>
                            <?php if ($propriedade['tipo_propriedade'] == 'Imovel'): ?>
                                <td><?php echo htmlspecialchars($propriedade['qtd_quartos']); ?></td>
                                <td><?php echo htmlspecialchars($propriedade['qtd_banheiros']); ?></td>
                                <td><?php echo htmlspecialchars($propriedade['qtd_suites']); ?></td>
                                <td><?php echo htmlspecialchars($propriedade['qtd_vagas_garagem']); ?></td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p>Você ainda não comprou nenhuma propriedade.</p>
        <?php endif; ?>
    </main>
    <?php require_once __DIR__ . '/../includes/footer.php'; ?>
</body>
</html>
