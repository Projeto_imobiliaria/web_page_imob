<?php
// public/processa_login.php
require_once __DIR__ . '/../includes/db.php';

$email = trim($_POST['email']);
$senha = trim($_POST['senha']);
$tipo_usuario = trim($_POST['tipo_usuario']); // 'cliente' ou 'corretor'

$conn = connect_db();
$table = $tipo_usuario === 'corretor' ? 'corretores' : 'clientes';

$stmt = $conn->prepare("SELECT id, nome, email, telefone, senha FROM $table WHERE email = ?");
$stmt->bind_param("s", $email);
$stmt->execute();
$stmt->store_result();

if ($stmt->num_rows > 0) {
    $stmt->bind_result($id, $nome, $email, $telefone, $hash);
    $stmt->fetch();

    if (password_verify($senha, $hash)) {
        session_start();
        $_SESSION['user_id'] = $id;
        $_SESSION['user_type'] = $tipo_usuario;
        $_SESSION['user_nome'] = $nome;
        $_SESSION['user_email'] = $email;
        $_SESSION['user_telefone'] = $telefone;
        header("Location: /public/index.php");
        exit;
    } else {
        $mensagem = "Senha incorreta.";
    }
} else {
    $mensagem = "Usuário não encontrado.";
}

$stmt->close();
$conn->close();

header("Location: /public/login.php?mensagem=" . urlencode($mensagem));
exit;
?>
