<?php
// public/remover_do_carrinho.php
require_once __DIR__ . '/../includes/auth.php';
require_once __DIR__ . '/../includes/header.php';
require_once __DIR__ . '/../includes/db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $propriedade_id = intval($_POST['propriedade_id']);

    $conn = connect_db();

    $stmt = $conn->prepare("DELETE FROM Carrinho WHERE propriedade_id = ?");
    $stmt->bind_param("i", $propriedade_id);

    if ($stmt->execute()) {
        $mensagem = "Propriedade removida do carrinho com sucesso.";
    } else {
        $mensagem = "Erro ao remover a propriedade do carrinho: " . $stmt->error;
    }

    $stmt->close();
    $conn->close();

    header("Location: /public/carrinho.php?mensagem=" . urlencode($mensagem));
    exit;
}
?>
